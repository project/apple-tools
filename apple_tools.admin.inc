<?php
/**
 * @file
 * The admin pages for apple tools module.
 */

/**
 * Administer device instances page.
 */
function apple_tools_settings_device_list() {
  $result = apple_tools_db_query("SELECT did, token, name, model, version, GMTtimezone FROM {apple_tools_device} ORDER BY did");
  $header = array(t('Token'), t('Name'), t('Model'), t('Version'), t('Timezone'), t('Edit'), t('Delete'));
  while ($data = db_fetch_object($result)) {
    $row = array();
    $row[] = check_plain($data->token);
    $row[] = check_plain($data->name);
    $row[] = check_plain($data->model);
    $row[] = check_plain($data->version);
    $row[] = check_plain($data->GMTtimezone);
    $row[] = l(t('edit'), "admin/settings/apple_tools/device/edit/$data->did");
    $row[] = l(t('delete'), "admin/settings/apple_tools/device/delete/$data->did");
    $rows[] = $row;
  }

  return theme('table', $header, $rows);
}

/**
 * Delete a device confirmation form.
 */
function apple_tools_form_device_delete(&$form_state, $device) {
  $form['type'] = array('#type' => 'hidden', '#value' => 'device');
  $form['did'] = array('#type' => 'hidden', '#value' => $device->did);
  $form['token'] = array('#type' => 'hidden', '#value' => $device->token);
  return confirm_form($form,
    t('Are you sure you want to delete the device with token of : @token?',
      array('@token' => $device->token)),
    'admin/settings/apple_tools/devices',
    t('Deleting a device will remove ability to send notifications to it. This action cannot be undone, but a device can register itself again.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler to delete a device after confirmation.
 */
function apple_tools_form_device_delete_submit($form, &$form_state) {
  apple_tools_delete_device($form_state['values']['did']);
  drupal_set_message(t('Deleted device @token.', array('@token' => $form_state['values']['token'])));
  watchdog(APPLE_TOOLS_WATCHDOG, 'Deleted device @token.', array('@token' => $form_state['values']['token']), WATCHDOG_NOTICE);
  $form_state['redirect'] = 'admin/settings/apple_tools/devices';
}

/**
 * Administer apps page.
 */
function apple_tools_settings_app_list() {
  $result = apple_tools_db_query("SELECT a.aid, a.app_id, a.version, a.certificate, a.status
    FROM {apple_tools_app} a
    ORDER BY aid");
  $header = array(t('App'), t('Version'), t('Certificate'), t('Status'), t('Devices'), t('Edit'), t('Delete'));
  while ($data = db_fetch_object($result)) {
    $device_count = db_result(apple_tools_db_query("SELECT COUNT(did) FROM {apple_tools_deviceapp}
      WHERE aid = %d AND status = %d", $data->aid, 1));
    $row = array();
    $row[] = check_plain($data->app_id);
    $row[] = check_plain($data->version);
    $row[] = check_plain($data->certificate);
    $row[] = check_plain($data->status);
    $row[] = check_plain($device_count);
    $row[] = l(t('edit'), "admin/settings/apple_tools/app/edit/$data->aid");
    $row[] = l(t('delete'), "admin/settings/apple_tools/app/delete/$data->aid");
    $rows[] = $row;
  }

  return theme('table', $header, $rows);
}

/**
 * Edit an app form.
 */
function apple_tools_form_app_edit(&$form_state, $app) {
  // Test for directories and create them if they don't exist.
  $apple_tools_dir = file_create_path('apple_tools');
  file_check_directory($apple_tools_dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
  $apple_tools_live_certs_dir = file_create_path('apple_tools/live-certs');
  file_check_directory($apple_tools_live_certs_dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
  $apple_tools_sandbox_certs_dir = file_create_path('apple_tools/sandbox-certs');
  file_check_directory($apple_tools_sandbox_certs_dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
  // Create a htaccess file to ensure people dont get to certificates.
  file_save_data("# Nobody can access this folder.\ndeny from all", $apple_tools_dir . '/.htaccess', $replace = FILE_EXISTS_REPLACE);

  // Create the form.
  $form['#attributes'] = array('enctype' => 'multipart/form-data');

  $form['type'] = array('#type' => 'hidden', '#value' => 'app');
  $form['aid'] = array('#type' => 'hidden', '#value' => $app->aid);
  $form['app_id'] = array('#type' => 'textfield', '#default_value' => $app->app_id, '#title' => t('Application name'));
  $form['version'] = array('#type' => 'textfield', '#default_value' => $app->version, '#title' => t('Application version'));
  $form['certificate_name'] = array('#type' => 'textfield', '#default_value' => $app->certificate, '#title' => t('Certificate name'));
  $form['status'] = array('#type' => 'checkbox', '#default_value' => $app->status, '#title' => t('Application is live'));
  $form['live-certificate'] = array('#type' => 'file', '#title' => t('Live certificate'));
  $form['sandbox-certificate'] = array('#type' => 'file', '#title' => t('Sandbox certificate'));

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save application'));

  return $form;
}

/**
 * Validate handler to edit an application.
 */
function apple_tools_form_app_edit_validate($form, &$form_state) {
  if ($form_state['values']['certificate_name'] == '') {
    form_set_error('certificate_name', t('You must select a certificate name for this application.'));
  }

  // If there is a certificate name set then save the certificates under this name.
  if ($form_state['values']['certificate_name'] != '') {
    // Add on a standard extention to the files if one is not present.
    if (drupal_substr($form_state['values']['certificate_name'], -4) != '.cer') {
      $form_state['values']['certificate_name'] = $form_state['values']['certificate_name'] . '.cer';
    }

    // Check to see if the certificate name is already used.
    if (db_result(apple_tools_db_query("SELECT 1 FROM {apple_tools_app} WHERE certificate = '%s' and aid != %d", $form_state['values']['certificate_name'], $form_state['values']['aid']))) {
      form_set_error('certificate_name', t('Certificate with that name already in the database, please choose another.'));
      return;
    }

    // Upload live certificate.
    apple_tools_upload_certificate($form_state['values']['certificate_name'], 'live-certificate', 'live', APPLE_TOOLS_PRODUCTION_CERTIFICATE_LOCATION);

    // Upload sandbox certificate.
    apple_tools_upload_certificate($form_state['values']['certificate_name'], 'sandbox-certificate', 'sandbox', APPLE_TOOLS_SANDBOX_CERTIFICATE_LOCATION);
  }
}

/**
 * Upload a certificate to the location specified.
 */
function apple_tools_upload_certificate($certificate_name, $certificate_field, $certificate_type = 'sandbox', $certificate_location = APPLE_TOOLS_SANDBOX_CERTIFICATE_LOCATION) {
  $validators = array(
    'file_validate_extensions' => array('cer pem'),
  );
  if ($file = file_save_upload($certificate_field, $validators)) {
    $destination = $certificate_location . $certificate_name;
    if (file_copy($file, $destination, FILE_EXISTS_REPLACE)) {
      drupal_set_message(t('The %certificate_type certificate was successfully uploaded', array('%certificate_type' => $certificate_type)));
    }
    else {
      form_set_error('live-certificate', t("Failed to upload the %certificate_type certificate; the %directory directory doesn't exist or is not writable.", array('%certificate_type' => $certificate_type, '%directory' => $certificate_location)));
    }
  }
}

/**
 * Submit handler to edit an application.
 */
function apple_tools_form_app_edit_submit($form, &$form_state) {
  $app = array(
    'aid' => $form_state['values']['aid'],
    'app_id' => $form_state['values']['app_id'],
    'version' => $form_state['values']['version'],
    'status' => $form_state['values']['status'],
    'certificate' => $form_state['values']['certificate_name'],
  );

  apple_tools_create_app($app);

  drupal_set_message(t('Saved app @id.', array('@id' => $form_state['values']['aid'])));
  watchdog(APPLE_TOOLS_WATCHDOG, 'Saved app @id.', array('@id' => $form_state['values']['aid']), WATCHDOG_NOTICE);
  $form_state['redirect'] = 'admin/settings/apple_tools/apps';
}

/**
 * Delete an app confirmation form.
 */
function apple_tools_form_app_delete(&$form_state, $app) {
  $form['type'] = array('#type' => 'hidden', '#value' => 'app');
  $form['aid'] = array('#type' => 'hidden', '#value' => $app->aid);
  $form['app_id'] = array('#type' => 'hidden', '#value' => $app->app_id);
  $form['version'] = array('#type' => 'hidden', '#value' => $app->version);
  return confirm_form($form,
    t('Are you sure you want to delete the app : @id[@version] ?',
      array('@id' => $app->app_id, '@version' => $app->version)),
    'admin/settings/apple_tools/apps',
    t('Deleting a app will remove ability to send notifications to it. This action cannot be undone, but an app can register itself again.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler to delete a app after confirmation.
 */
function apple_tools_form_app_delete_submit($form, &$form_state) {
  apple_tools_delete_app($form_state['values']['aid']);
  drupal_set_message(t('Deleted app @id.', array('@id' => $form_state['values']['aid'])));
  watchdog(APPLE_TOOLS_WATCHDOG, 'Deleted app @id.', array('@id' => $form_state['values']['aid']), WATCHDOG_NOTICE);
  $form_state['redirect'] = 'admin/settings/apple_tools/apps';
}

/**
 * Menu callback; Provide the administration overview page.
 */
function apple_tools_settings_page() {
  $roles = array_merge(array(NULL => t('None set')), user_roles());

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => TRUE,
    '#description' => t('General settings')
  );

  $form['general']['apple_tools_shared_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Shared secret to be used with receipt validation'),
    '#default_value' => check_plain(variable_get('apple_tools_shared_secret', '')),
    '#description' => t('This is the secret used for the apple itunes connect to verify receipts'),
  );

  $form['general']['apple_tools_default_role'] = array(
    '#type' => 'select',
    '#title' => t('The role that is given to a valid receipt'),
    '#options' => $roles,
    '#default_value' => variable_get('apple_tools_default_role', ''),
    '#description' => t('This is the role thats assigned when an item is first created.'),
  );

  $form['general']['apple_tools_default_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('The duration that is given as default if a new item is inserted'),
    '#default_value' => variable_get('apple_tools_default_duration', 1),
    '#description' => t('This is the duration thats assigned when a item is first created.'),
  );

  $form['general']['apple_tools_always_use_sandbox'] = array(
    '#type' => 'select',
    '#title' => t('Always use the sandbox settings'),
    '#default_value' => variable_get('apple_tools_always_use_sandbox', 'yes'),
    '#options' => array('no' => t('No'), 'yes' => t('Yes')),
    '#weight' => -1,
  );

  $form['production'] = array(
    '#type' => 'fieldset',
    '#title' => t('Production Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Production settings')
  );

  $form['production']['apple_tools_verify_receipt_url'] = array(
    '#type' => 'textfield',
    '#title' => t("Verify receipt uri"),
    '#default_value' => check_plain(variable_get('apple_tools_verify_receipt_url', 'https://sandbox.itunes.apple.com/verifyReceipt')),
  );

  $form['production']['apple_tools_production_apns_gateway'] = array(
    '#type' => 'textfield',
    '#title' => t("APNS gateway uri"),
    '#default_value' => check_plain(variable_get('apple_tools_production_apns_gateway', 'ssl://gateway.push.apple.com:2195')),
  );

  $form['production']['apple_tools_production_apns_feedback_service'] = array(
    '#type' => 'textfield',
    '#title' => t("APNS feedback service uri"),
    '#default_value' => check_plain(variable_get('apple_tools_production_apns_feedback_service', 'ssl://feedback.push.apple.com:2196')),
  );

  $form['production']['apple_tools_production_certificate_location'] = array(
    '#type' => 'textfield',
    '#title' => t("Certificate location"),
    '#default_value' => check_plain(variable_get('apple_tools_production_certificate_location',
      file_directory_path() . '/apple_tools/production-certs/')),
  );

  $form['sandbox'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sandbox Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Sandbox settings')
  );

  $form['sandbox']['apple_tools_sandbox_verify_receipt_url'] = array(
    '#type' => 'textfield',
    '#title' => t("Verify receipt uri"),
    '#default_value' => check_plain(variable_get('apple_tools_sandbox_verify_receipt_url', 'https://sandbox.itunes.apple.com/verifyReceipt')),
  );

  $form['sandbox']['apple_tools_sandbox_apns_gateway'] = array(
    '#type' => 'textfield',
    '#title' => t("APNS gateway uri"),
    '#default_value' => check_plain(variable_get('apple_tools_sandbox_apns_gateway', 'ssl://gateway.sandbox.push.apple.com:2195')),
  );
  
  $form['sandbox']['apple_tools_sandbox_apns_feedback_service'] = array(
    '#type' => 'textfield',
    '#title' => t("APNS feedback service uri"),
    '#default_value' => check_plain(variable_get('apple_tools_sandbox_apns_feedback_service', 'ssl://feedback.sandbox.push.apple.com:2196')),
  );
  
  $form['sandbox']['apple_tools_sandbox_certificate_location'] = array(
    '#type' => 'textfield',
    '#title' => t("Certificate location"),
    '#default_value' => check_plain(variable_get('apple_tools_sandbox_certificate_location',
      file_directory_path() . '/apple_tools/sandbox-certs/')),
  );

  return system_settings_form($form);
}
