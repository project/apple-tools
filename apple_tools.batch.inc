<?php
/**
 * @file
 * Batch functions for apple_tools module.
 */
define('APPLE_TOOLS_BATCH_ADD_MESSAGE_SIZE', 2000);

/**
 * Queue messages for devices for apps in a batch.
 */
function apple_tools_queue_messages_batch($app, $delivery, $alert, $badge, $sound, $custom, &$context) {
  if (empty($context['sandbox'])) {
    // Is this message going to all apps? or specific ones?
    if ($app != 'ALL') {
      $apps = explode(',', $app);
    }
    else {
      // This message is to go to all apps.
      $apps = apple_tools_retrieve_aid_with_certificate();
    }

    // Setup the placeholders string. 
    $placeholders =  db_placeholders($apps);

    // Get the devices this should be run for.
    $query = "SELECT DISTINCT(d.did) AS device_id 
      FROM {apple_tools_device} d, {apple_tools_deviceapp} da 
      WHERE d.did = da.did 
      AND da.status = 1 
      AND da.aid IN ($placeholders) 
      ORDER BY d.did";

    // Get the max number of records to be processed.
    $max = db_result(apple_tools_db_query("SELECT COUNT(DISTINCT(d.did)) AS device_id 
      FROM {apple_tools_device} d, {apple_tools_deviceapp} da 
      WHERE d.did = da.did
      AND da.status = 1
      AND da.aid IN ($placeholders) 
      ORDER BY d.did", $apps));

    // Initalise the context
    $context['sandbox']['apps'] = $apps;
    $context['sandbox']['query'] = $query;
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = $max;
    $context['sandbox']['drush'] = function_exists('drush_print');
  }
  $batch_size = variable_get('apple_tools_message_queue_batch_size', APPLE_TOOLS_BATCH_ADD_MESSAGE_SIZE);

  $devices = array();

  // Get the next set of devices.
  $result = apple_tools_db_query_range($context['sandbox']['query'], $context['sandbox']['apps'], $context['sandbox']['progress'], $batch_size);
  while ($row = db_fetch_array($result)) {
    $devices[] = $row['device_id'];
  }

  for ($x = 0; $x < $batch_size && $context['sandbox']['progress'] < $context['sandbox']['max']; $x++, $context['sandbox']['progress']++) {

    // Get the device that is being sent the message.
    $device_id = $devices[$x];

    foreach ($context['sandbox']['apps'] as $app_id) {
      // We are in the loop of apps for devices.
      $result = apple_tools_submit_message_to_device_for_app($device_id, $app_id, $delivery, $alert, $badge, $sound, $custom);
      if (!empty($result) && variable_get('apple_tools_debug', FALSE)) {
        watchdog(APPLE_TOOLS_WATCHDOG, '@result for device @did and app @app_id.', array('@result' => $result, '@did' => $device_id, '@app_id' => $app_id));
      }
    }
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    if ($context['sandbox']['drush']) {
      drush_print(dt(
        '@count messages queued, @percent% complete.',
        array('@count' => $context['sandbox']['progress'], '@percent' => number_format($context['finished'] * 100, 2))
      ));
    }
  }
}

/**
 * Report on finished batch job.
 */
function apple_tools_queue_messages_finished($success, $results, $operations) {
  $t = function_exists('dt') ? 'dt' : 't';
  $log = function_exists('drush_log') ? 'drush_log' : 'drupal_set_message';
  $message = $t('There was a problem queuing the messsages to be sent.');
  $status = 'error';
  if ($success) {
    $message = $t('All messages have been queued.');
    $status = ($log == 'drush_log') ? 'ok' : 'status';
  }

  $log($message, $status);
}

