<?php
/**
 * @file
 * CRUD functions for apple_tools module.
 */


/**
 * Create the app entry in the db from the supplied structure.
 */
function apple_tools_create_app($app) {
  watchdog(APPLE_TOOLS_WATCHDOG, 'Create app');
  $result = db_result(apple_tools_db_query("SELECT a.aid
    FROM {apple_tools_app} a
    WHERE a.app_id = '%s'
    AND a.version = '%s'",
    $app['app_id'], $app['version']));
  if ($result) {
    $app['aid'] = $result;
    watchdog(APPLE_TOOLS_WATCHDOG, 'Existing application : @aid', array('@aid' => $app['aid']));
    $app['changed'] = time();
    drupal_write_record('apple_tools_app', $app, 'aid');
  }
  else {
    watchdog(APPLE_TOOLS_WATCHDOG, 'New application, will register - app_id:@app_id, version:@version', array('@app_id' => $app['app_id'], '@version' => $app['version']));

    $app['aid'] = NULL;
    $app['created'] = time();
    $app['changed'] = time();
    drupal_write_record('apple_tools_app', $app);
  }
  return $app['aid'];
}

/**
 * Retrive the app(s) from the tables.
 */
function apple_tools_retrieve_app($app_id = NULL) {
  $sql = "SELECT a.aid, a.app_id, a.version, a.certificate, a.status
    FROM {apple_tools_app} a";
  if (!empty($app_id))  {
    $sql .= " WHERE a.app_id = '%s'";
    $result = apple_tools_db_query($sql, $app_id);
  }
  else {
    $result = apple_tools_db_query($sql);
  }
  $apps = array();
  while ($row = db_fetch_array($result)) {
    $apps[] = $row;
  }
  return $apps;
}

/**
 * Retrive the app(s) from the tables that have a certificate set.
 */
function apple_tools_retrieve_aid_with_certificate() {
  $result = apple_tools_db_query("SELECT a.aid FROM {apple_tools_app} a WHERE certificate != ''");
  $apps = array();
  while ($row = db_fetch_array($result)) {
    $apps[] = $row['aid'];
  }

  return $apps;
}

/**
 * Retrive the app(s) from the tables from the aid.
 */
function apple_tools_retrieve_app_from_aid($aid = NULL) {
  $sql = "SELECT a.aid, a.app_id, a.version, a.certificate, a.status
    FROM {apple_tools_app} a";
  if (!empty($aid)) {
    $sql .= " WHERE a.aid = %d";
    $result = apple_tools_db_query($sql, $aid);
  }
  else {
    $result = apple_tools_db_query($sql);
  }
  $apps = array();
  while ($row = db_fetch_array($result)) {
    $apps[] = $row;
  }
  return $apps;
}


/**
 * Create a new device, if its not already in the table.
 */
function apple_tools_create_device($device) {
  watchdog(APPLE_TOOLS_WATCHDOG, 'apple_tools_create_device');
  $result = db_result(apple_tools_db_query("SELECT d.did
    FROM {apple_tools_device} d
    WHERE d.token = '%s'", $device['token']));
  if ($result) {
    $device['did'] = $result;
    watchdog(APPLE_TOOLS_WATCHDOG, 'Existing device : @did', array('@did' => $device['did']));
    $device['changed'] = time();
    drupal_write_record('apple_tools_device', $device, 'did');
  }
  else {
    watchdog(APPLE_TOOLS_WATCHDOG, 'New device : @token', array('@token' => $device['token']));
    $device['did'] = NULL;
    $device['created'] = time();
    $device['changed'] = time();
    drupal_write_record('apple_tools_device', $device);
  }
  return $device['did'];
}

/**
 * Disable Apple device.
 *
 * This gets called automatically when Apple's Feedback Service responds with an invalid token.
 *
 * @param sting $token 64 character unique device token tied to device id
 */
function apple_tools_disable_device($token) {
  db_query("UPDATE {apple_tools_deviceapp}
    SET status = %d, changed = %d
    WHERE did IN (
      SELECT did FROM {apple_tools_device} WHERE token = '%s'
    )", 0, time(), $token);
}

/**
 * Delete Apple device.
 *
 * This gets called to delete a device from the table, it will delete
 * associations to messages and apps also.
 *
 * @param sting $did
 */
function apple_tools_delete_device($did) {
  // Delete unsent messages to this device.
  db_query("DELETE FROM {apple_tools_message}
    WHERE did = %d AND status = %d", $did, 0);
  // Delete the association with the apps for this device.
  db_query("DELETE FROM {apple_tools_deviceapp}
    WHERE did = %d", $did);
  // Delete the device from the device table.
  db_query("DELETE FROM {apple_tools_device}
    WHERE did = %d", $did);
}

/**
 * Delete Apple app.
 *
 * This gets called to delete a app from the tables, it will delete
 * associations to messages and devices.
 *
 * @param sting $aid
 */
function apple_tools_delete_app($aid) {
  // Delete unsent messages to this app.
  db_query("DELETE FROM {apple_tools_message}
    WHERE aid = %d AND status = %d", $aid, 0);
  // Delete the association with the devices for this app.
  db_query("DELETE FROM {apple_tools_deviceapp}
    WHERE aid = %d", $aid);
  // Delete the app from the app table.
  db_query("DELETE FROM {apple_tools_app}
    WHERE aid = %d", $aid);
}

/**
 * Retrive the device from the database.
 */
function apple_tools_retrieve_device($device_id = NULL) {
  $sql = "SELECT d.did, d.token, d.name, d.model, d.version, d.GMTtimezone as timezone_offset FROM {apple_tools_device} d";
  if (!empty($device_id)) {
    $sql .= " WHERE d.did = %d";
    $result = apple_tools_db_query($sql, $device_id);
  }
  else {
    $result = apple_tools_db_query($sql);
  }
  $devices = array();
  while ($row = db_fetch_array($result)) {
    $devices[] = $row;
  }
  return $devices;
}

/**
 * Function to register a device with an app.
 */
function apple_tools_create_deviceapp($deviceapp) {
  watchdog(APPLE_TOOLS_WATCHDOG, 'Create a new device app association.');

  if ($deviceapp['pushalert'] == 'enabled' || $deviceapp['pushalert'] === 1 || $deviceapp['pushalert'] === TRUE) {
    $deviceapp['pushalert'] = 1;
  }
  elseif ($deviceapp['pushalert'] == 'disabled' || $deviceapp['pushalert'] === 0 || $deviceapp['pushalert'] === FALSE) {
    $deviceapp['pushalert'] = 0;
  }

  if ($deviceapp['pushbadge'] == 'enabled' || $deviceapp['pushbadge'] === 1 || $deviceapp['pushbadge'] === TRUE) {
    $deviceapp['pushbadge'] = 1;
  }
  elseif ($deviceapp['pushbadge'] == 'disabled' || $deviceapp['pushbadge'] === 0 || $deviceapp['pushbadge'] === FALSE) {
    $deviceapp['pushbadge'] = 0;
  }

  if ($deviceapp['pushsound'] == 'enabled' || $deviceapp['pushsound'] === 1 || $deviceapp['pushsound'] === TRUE) {
    $deviceapp['pushsound'] = 1;
  }
  elseif ($deviceapp['pushsound'] == 'disabled' || $deviceapp['pushsound'] === 0 || $deviceapp['pushsound'] === FALSE) {
    $deviceapp['pushsound'] = 0;
  }

  watchdog(APPLE_TOOLS_WATCHDOG, 'Existing application device:app : @aid:@did', array('@aid' => $deviceapp['aid'], '@did' => $deviceapp['did']));

  $result = db_result(apple_tools_db_query("SELECT r.daid
    FROM {apple_tools_deviceapp} r
    WHERE r.did = %d
    AND r.aid = %d",
    $deviceapp['did'], $deviceapp['aid']));
  if ($result) {
    $deviceapp['daid'] = $result;
    watchdog(APPLE_TOOLS_WATCHDOG, 'Existing application device id : @daid', array('@daid' => $deviceapp['daid']));
    $deviceapp['changed'] = time();
    drupal_write_record('apple_tools_deviceapp', $deviceapp, 'daid');
  }
  else {
    $deviceapp['created'] = time();
    $deviceapp['changed'] = time();
    drupal_write_record('apple_tools_deviceapp', $deviceapp);
  }
  return $deviceapp['daid'];
}

/**
 * Retrive the data associated with a device and app.
 */
function apple_tools_retrieve_deviceapp($did, $aid) {
  $result = apple_tools_db_query("SELECT r.daid, r.status, r.pushbadge, r.pushsound,r.pushalert
    FROM {apple_tools_deviceapp} r
    WHERE r.did = '%s'
    AND r.aid = '%s'",
    $did, $aid);
  return db_fetch_array($result);
}

/**
 * Create a receipt.
 */
function apple_tools_create_receipt($receipt, $raw_receipt) {
  $result = db_result(apple_tools_db_query("SELECT r.rid
    FROM {apple_tools_receipt} r
    WHERE r.transaction_id = '%s'
    AND r.product_id = '%s'
    AND r.raw = '%s'",
    $receipt['receipt']['transaction_id'], $receipt['receipt']['product_id'], $raw_receipt));
  if ($result) {
    return $result;
  }
  // This record is not already inserted,
  // so insert and return the new receipt ID.
  $receipt['receipt']['created'] = time();
  $receipt['receipt']['raw'] = $raw_receipt;
  drupal_write_record('apple_tools_receipt', $receipt['receipt']);
  return $receipt['receipt']['rid'];
}

/**
 * Create an item associated with an app.
 */
function apple_tools_create_app_item($app_item_id, $app_id, $app_version) {
  $result = db_result(apple_tools_db_query("SELECT p.pid
    FROM {apple_tools_app_item} p,
    {apple_tools_app} a
    WHERE p.app_item_id = '%s'
    AND p.aid = a.aid
    AND a.app_id = '%s'
    AND a.version = '%s'",
    $app_item_id, $app_id, $app_version));
  if (!$result) {
    // This record is not already inserted,
    // so insert and return the new product ID.
    $app = array(
      'app_id' => $app_id,
      'version' => $app_version,
      'created' => time(),
      'changed' => time(),
    );

    $aid = apple_tools_create_app($app);

    if ($aid) {
      $app_item = array(
        'app_item_id' => $app_item_id,
        'aid' => $aid,
        'created' => time(),
        'changed' => time(),
      );

      $result = drupal_write_record('apple_tools_app_item', $app_item);

      if ($result) {
        return $app_item['pid'];
      }
    }
    return FALSE;
  }
  return $result;
}

/**
 * Retrieve the app items as a db_query object.
 */
function apple_tools_retrieve_app_items() {
  return apple_tools_db_query("SELECT app_item_id as product_name, pid FROM {apple_tools_app_item} ORDER BY app_item_id ASC");
}
