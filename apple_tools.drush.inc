<?php
/**
 * @file
 * Drush support for the apple_tools module.
 */

/**
 * Implementation of hook_drush_command().
 */
function apple_tools_drush_command() {
  $items = array();

  $items['apple-tools-push-messages'] = array(
    'description' => 'Processes a batch of messages using the Apple Tools method.',
    'aliases' => array('at-push'),
  );
  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function apple_tools_drush_help($section) {
  switch ($section) {
    case 'drush:apple-tools-cron':
      return dt("Run apple tools cron hook.");
      break;
  }
}

/**
 * Implementation of drush_apple_tools_push_messages().
 */
function drush_apple_tools_push_messages() {
  apple_tools_push_messages();
}

