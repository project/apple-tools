<?php
/**
 * @file
 * This contains the form that allows us to add a new message.
 */

/**
 * New message form.
 */
function apple_tools_new_message_form($form_state) {
  $form = array();

  $form['message'] = array(
    '#type' => 'textfield',
    '#title' => t('Please enter your message'),
    '#description' => t('This is the message that the users will recieve.'),
  );

  $form['sound'] = array(
    '#type' => 'textfield',
    '#title' => t('Do you want the message to have sound alert (sound file)?'),
    '#description' => t('The sound file has to be inside your app bundle to be played.'),
  );

  $form['action-loc-key'] = array(
    '#type' => 'textfield',
    '#title' => t('Do you want the action-local -key  to alert (string)?'),
  );

  $form['loc-key'] = array(
    '#type' => 'textfield',
    '#title' => t('Do you want the local -key  to alert (string)?'),
  );

  $form['loc-args'] = array(
    '#type' => 'textfield',
    '#title' => t('Do you want the localargs  to alert (string)?'),
  );

  $form['badge'] = array(
    '#type' => 'textfield',
    '#title' => t('What do you want the message to show a badge (int)?'),
  );

  $form['deliver'] = array(
    '#type' => 'textfield',
    '#title' => t('Please enter your timestamp for intended delivery (unix timestamp value)'),
  );

  $form['content-available'] = array(
    '#type' => 'radios',
    '#title' => t('Is there new content avaliable, for newsstand'),
    '#required' => TRUE,
    '#options' => array( 0 => 'No', 1 => 'Yes'),
    '#description' => t('This will alert an app that supports the newsstand kit framework, to start to download new content in the background'),
  );

  $form['device'] = array(
    '#type' => 'textfield',
    '#title' => t('Please enter your device'),
    '#required' => TRUE,
    '#description' => t('Enter ALL to send to all registered devices.'),
  );

  $form['app'] = array(
    '#type' => 'select',
    '#title' => t('Please enter your app'),
    '#required' => TRUE,
    '#options' => apple_tools_build_apps_array(),
    '#description' => t('Select an individual app to send the message to, or all iOS applications that are registered.  Sandbox indicates this application is operating in a non live state.  If the application is not in the list, its probibly not got a certificate associated with it. ') . l('manage apps', 'admin/settings/apple_tools/apps'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send message to queue'),
  );

  return $form;
}

/**
 * Form has been submitted process it.
 */
function apple_tools_new_message_form_submit($form, &$form_state) {
  // Create the alert structure.
  $alert = array(
    'alert' => $form_state['values']['message'],
    'actionlockey' => $form_state['values']['action-loc-key'],
    'lockey' => $form_state['values']['loc-key'],
    'locargs' => $form_state['values']['loc-args'],
    'launch-image' => $form_state['values']['launch-image'], // iOS 4 addition.
    'content-available' => $form_state['values']['content-available'], // iOS 5 addition.
  );

  // Define a custom array for custom key values to be passed to the application.
  // This is an example, of some custom structure to pass.
  //
  // $custom = array(
  //   'acme1' => array(
  //      'identifier' => 43,
  //      'description' => 'Test description',
  //   ),
  //   'acme2' => array(
  //      'list' => '4, 3, 6, 2',
  //      'excluded' => array(1, 3, 45, 6, 89, 4),
  //   ),
  // );
  //
  // Then you would pass this value in to the apple_tools_submit_message() function.

  if (empty($form_state['values']['deliver'])) {
    $time_to_deliver = time();
  }
  else {
    $time_to_deliver = $form_state['values']['deliver'];
  }

  $output = apple_tools_submit_message($form_state['values']['device'], $form_state['values']['app'], $time_to_deliver, $alert, $form_state['values']['badge'], $form_state['values']['sound']);

  if (empty($output)) {
    $output = 'No valid device/apps were found to queue the message to.';
  }

  drupal_set_message($output);
}

/**
 * Helper function to build a usable array for selecting applications.
 */
function apple_tools_build_apps_array() {
  $apps = array(
    NULL => t('Please select a app.'),
  );

  $apps['ALL'] = t('All iOS applications');

  $query = apple_tools_db_query("SELECT aid, IF(status = %d, concat_ws(' - ', app_id, version), concat_ws(' - ', app_id, version, '%s')) AS name FROM {apple_tools_app} WHERE app_id != '' AND certificate != '' ORDER BY name ASC", APPLE_TOOLS_APP_ACTIVE, APPLE_TOOLS_APP_SANDBOX);
  while ($app = db_fetch_object($query)) {
    $apps[$app->aid] = $app->name;
  }

  return $apps;
}

