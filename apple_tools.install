<?php
/**
 * @file
 * Install and uninstall functions as well as schema definition for the apple_tools module.
 */

/**
 * Implementation of hook_schema().
 */
function apple_tools_schema() {
  $schema['apple_tools_device'] = array(
    'description' => 'These are devices that have registered with the service, against an app.',
    'fields' => array(
      'did' => array(
        'description' => 'The $did of the device.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'token' => array(
        'description' => 'Unique token of the device for a specific app.',
        'type' => 'char',
        'length' => 64,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => "The name of the device.",
        'type' => 'varchar',
        'length' => 255,
      ),
      'model' => array(
        'description' => "The model of the device.",
        'type' => 'varchar',
        'length' => 100,
      ),
      'version' => array(
        'description' => "The version of the device.",
        'type' => 'varchar',
        'length' => 25,
      ),
      'GMTtimezone' => array(
        'description' => "The timezone offset from GMT of the device.",
        'type' => 'int',
        'null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the data was created.',
        'type' => 'int', //not using datetime so the as non of the formatting code works with datetime (as its returned as string)
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the data was changed.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'node_created'        => array('created'),
      ),
    'unique keys' => array(
      'did_token' => array('did', 'token'),
      'token'     => array('token'),
      ),
    'primary key' => array('did'),
  );

  $schema['apple_tools_message'] = array(
    'description' => 'Stores the messages that are to be sent to the notification server.',
    'fields' => array(
      'mid' => array(
        'description' => 'The $mid of the message.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'message' => array(
        'description' => 'The message that is to be sent.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'did' => array(
        'description' => 'A device that this message is intended to be sent to.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'aid' => array(
        'description' => 'The app that this message is intended to be sent to.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'status' => array(
        'description' => 'The status of the message. - 0:queued,1:delivered,2:failed',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'delivery' => array(
        'description' => 'The Unix timestamp when the data should be delivered.',
        'type' => 'int', //not using datetime so the as non of the formatting code works with datetime (as its returned as string)
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the data was created.',
        'type' => 'int', //not using datetime so the as non of the formatting code works with datetime (as its returned as string)
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the data was changed.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'aid'        => array('aid'),
      'did'        => array('did'),
      ),
    'primary key' => array('mid'),
  );

  $schema['apple_tools_receipt'] = array(
    'description' => 'Store the receipt from a successful payment taken from the store.',
    'fields' => array(
      'rid' => array(
        'description' => 'The $rid of the receipt.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'quantity' => array(
        'description' => 'The number of items purchased. This value corresponds to the quantity property of the SKPayment object stored in the transaction’s payment property.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'product_id' => array(
        'description' => 'The product identifier of the item that was purchased. This value corresponds to the productIdentifier property of the SKPayment object stored in the transaction’s payment property.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'transaction_id' => array(
        'description' => 'The transaction identifier of the item that was purchased. This value corresponds to the transaction’s transactionIdentifier property.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'purchase_date' => array(
        'description' => 'The date and time this transaction occurred. This value corresponds to the transaction’s transactionDate property.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'original_transaction_id' => array(
        'description' => 'For a transaction that restores a previous transaction, this holds the original transaction identifier.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'original_purchase_date' => array(
        'description' => 'For a transaction that restores a previous transaction, this holds the original purchase date.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'app_item_id' => array(
        'description' => 'A string that the App Store uses to uniquely identify the iOS application that created the payment transaction. If your server supports multiple iOS applications, you can use this value to differentiate between them. Applications that are executing in the sandbox do not yet have an app-item-id assigned to them, so this key is missing from receipts created by the sandbox.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'version_external_identifier' => array(
        'description' => 'An arbitrary number that uniquely identifies a revision of your application. This key is missing in receipts created by the sandbox.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'bid' => array(
        'description' => 'The bundle identifier for the iOS application.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'bvrs' => array(
        'description' => 'A version number for the iOS application.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the data was created.',
        'type' => 'int', //not using datetime so the as non of the formatting code works with datetime (as its returned as string)
        'not null' => TRUE,
        'default' => 0,
      ),
      'raw' => array(
        'description' => 'This is the raw receipt that is encoded in base64',
        'type' => 'text',
        'size' => 'medium',
        'not null' => TRUE,
      ),
      'uid' => array(
        'description' => 'This is the user id of the receipt, it can be null',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('rid'),
  );

  $schema['apple_tools_app'] = array(
    'description' => 'Stores the apps that are to be sent messages via the notification server.',
    'fields' => array(
      'aid' => array(
        'description' => 'The $aid of the message.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'app_id' => array(
        'description' => 'A string that the App Store uses to uniquely identify the iOS application.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'version' => array(
        'description' => 'The version of the app (so we can target different versions if needed).',
        'type' => 'varchar',
        'length' => '25',
        'not null' => TRUE,
        'default' => '',
      ),
      'certificate' => array(
        'description' => 'The certificate to use with this app. (location on the sever)',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'status' => array(
        'description' => 'The status of this application. 1:live,0:sandbox',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the data was created.',
        'type' => 'int', //not using datetime so the as non of the formatting code works with datetime (as its returned as string)
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the data was changed.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),

    ),
    'primary key' => array('aid'),
    'unique keys' => array('app_id_version' => array('app_id', 'version')),
  );

  $schema['apple_tools_app_item'] = array(
    'description' => 'Stores the app products avaliable for a application (in app purchases).',
    'fields' => array(
      'pid' => array(
        'description' => 'The $pid of the product.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'aid' => array(
        'description' => 'The $aid of the products parent application.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'app_item_id' => array(
        'description' => 'A string that the App Store uses to uniquely identify the iOS application item.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '',
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the data was created.',
        'type' => 'int', //not using datetime so the as non of the formatting code works with datetime (as its returned as string)
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the data was changed.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),

    ),
    'primary key' => array('pid'),
    'unique keys' => array('aid_app_item_id' => array('aid', 'app_item_id')),
  );


  $schema['apple_tools_deviceapp'] = array(
    'description' => 'Stores what app is on what device and what notifications can be sent.',
    'fields' => array(
      'daid' => array(
        'description' => 'The $daid of the device app.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'did' => array(
        'description' => 'The $did of the device.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'aid' => array(
        'description' => 'The $aid of the app.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => 'The status of this application. 1:installed,0:uninstalled',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'pushbadge' => array(
        'description' => 'The status of this push (badge). 0:disabled,1:enabled',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'pushalert' => array(
        'description' => 'The status of this push (alert). 0:disabled,1:enabled',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'pushsound' => array(
        'description' => 'The status of this push (sound). 0:disabled,1:enabled',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the data was created.',
        'type' => 'int', //not using datetime so the as non of the formatting code works with datetime (as its returned as string)
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the data was changed.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'app_id'        => array('aid'),
      'device_id'        => array('did'),
      ),
    'unique keys' => array(
      'did_aid' => array('did', 'aid'),
      ),
    'primary key' => array('daid'),
  );

  return $schema;
}

/**
 * Implementation of hook_install().
 */
function apple_tools_install() {
  drupal_install_schema('apple_tools');
  variable_set('apple_tools_feedback_url', 'ssl://feedback.push.apple.com:2196');
  variable_set('apple_tools_ssl_url', 'ssl://gateway.push.apple.com:2195');
  variable_set('apple_tools_verify_receipt_url', 'https://buy.itunes.apple.com/verifyReceipt');
  variable_set('apple_tools_sandbox_feedback_url', 'ssl://feedback.sandbox.push.apple.com:2196');
  variable_set('apple_tools_sandbox_ssl_url', 'ssl://gateway.sandbox.push.apple.com:2195');
  variable_set('apple_tools_sandbox_verify_receipt_url', 'https://sandbox.itunes.apple.com/verifyReceipt');
}

/**
 * Implementation of hook_uninstall().
 */
function apple_tools_uninstall() {
  variable_del('apple_tools_feedback_url');
  variable_del('apple_tools_ssl_url');
  variable_del('apple_tools_verify_receipt_url');
  variable_del('apple_tools_sandbox_feedback_url');
  variable_del('apple_tools_sandbox_ssl_url');
  variable_del('apple_tools_sandbox_verify_receipt_url');
  drupal_uninstall_schema('apple_tools');
}

/**
 * Add the raw and uid column to the receipt table.
 */
function apple_tools_update_6000() {
  $ret = array();
  $field_specification = array(
    'description' => 'This is the raw receipt that is encoded in base64',
    'type' => 'text',
    'size' => 'medium',
    'not null' => TRUE,
  );

  db_add_field($ret, 'apple_tools_receipt', 'raw', $field_specification);

  $field_specification = array(
    'description' => 'This is the user id of the receipt, it can be null',
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => FALSE,
  );

  db_add_field($ret, 'apple_tools_receipt', 'uid', $field_specification);

  return $ret;
}

/**
 * Update the app_item and app table to have a unique composite keys.
 */
function apple_tools_update_6001() {
  $ret = array();
  // Can not use the db_add_unique_key function as records are already duplicated.
  $ret[] = update_sql("ALTER IGNORE TABLE {apple_tools_app_item} ADD UNIQUE KEY aid_app_item_id (aid, app_item_id)");
  $ret[] = update_sql("ALTER IGNORE TABLE {apple_tools_app} ADD UNIQUE KEY app_id_version (app_id, version)");
      
  return $ret;
}
