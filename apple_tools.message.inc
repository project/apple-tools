<?php
/**
 * @file
 * Message page callbacks for apple_tools module.
 */

define('APPLE_TOOLS_MESSAGE_NOT_PROCESSED', 0);
define('APPLE_TOOLS_MESSAGE_SUCCESS', 1);
define('APPLE_TOOLS_MESSAGE_FAILURE', 2);
define('APPLE_TOOLS_MESSAGE_DEVICE_APP_STATUS_OK', 1);
define('APPLE_TOOLS_SEND_MESSAGE_LIMIT', 1000);

/**
 * Create a message in the table thats waiting to be sent to the noted device.
 *
 * @param $message
 *   The message in a json format, and in the apple structure.
 * @param $app_id
 *   The id of the appliction that this message is for.
 * @param $device_id
 *   The id of the device that this message is for.
 * @param $delivery
 *   The timestamp that this message should be delivered (GMT based).
 *
 * @return
 *   The message_id of the new record, or the existing message_id if its
 *   already in the database.
 */
function apple_tools_create_message($message, $app_id, $device_id, $delivery) {
  // Is this device enabled for this application?
  $device_app = apple_tools_retrieve_deviceapp($device_id, $app_id);
  if ($device_app['status'] != 1) {
    return;
  }
  $result = db_result(apple_tools_db_query("SELECT m.mid
    FROM {apple_tools_message} m
    WHERE m.aid = %d
    AND m.message = '%s'
    AND m.did = %d
    AND m.status = %d", $app_id, $message, $device_id, APPLE_TOOLS_MESSAGE_NOT_PROCESSED));
  if (!$result) {
    // This record is not already inserted, so insert and return the new message ID.
    $msg = array(
      'aid' => $app_id,
      'did' => $device_id,
      'message' => $message,
      'delivery' => $delivery,
      'created' => time(),
      'changed' => time(),
    );
    $result = drupal_write_record('apple_tools_message', $msg);
    if ($result) {
      return $msg['mid'];
    }
  }
  return $result;
}

/**
 * Retrieve a message(s) that are currently in the database.
 *
 * @param $msg_status
 *   The message status defaulted to 0 [queued].
 * @param $status
 *   The status of the device/app, 0:disabled 1:enabled.
 *
 * @return
 *   An array of messages.
 */
function apple_tools_retrieve_message($app_id = NULL, $msg_status = APPLE_TOOLS_MESSAGE_NOT_PROCESSED, $status = APPLE_TOOLS_MESSAGE_DEVICE_APP_STATUS_OK, $limit = APPLE_TOOLS_SEND_MESSAGE_LIMIT) {
  // Only send one message per user... oldest message first.
  $sql = "SELECT m.mid, m.message, d.token
    FROM {apple_tools_message} m,
      {apple_tools_device} d,
      {apple_tools_deviceapp} da
    WHERE m.status = %d
      AND m.delivery <= %d
      AND m.aid = %d
      AND m.did = d.did
      AND m.aid = da.aid
      AND m.did = da.did
      AND da.status = %d
    GROUP BY d.did
    ORDER BY m.created ASC
    LIMIT %d";

  $result = apple_tools_db_query($sql, $msg_status, time(), $app_id, $status, $limit);

  $messages = array();
  while ($row = db_fetch_array($result)) {
    $message = array(
      'mid' => $row['mid'],
      'message' => $row['message'],
      'token' => $row['token'],
    );
    $messages[] = $message;
  }

  return $messages;

}

/**
 * Check Feedback; check the feedback channel from apple.
 *    If a device token is returned that device will be disabled so we
 *    do not keep sending messages for a device that may of had the
 *    application removed.
 *
 * @param $live
 *   A flag to say if to use apples live service.
 * @param $certificate
 *   The path of the certificate file to be used
 *
 */
function apple_tools_check_feedback($live, $certificate) {
  if (empty($live) || empty($certificate)) {
    return;
  }
  $opts = array('ssl' => array('local_cert' => $certificate, 'verify_peer' => FALSE));
  $ctx = stream_context_create($opts);

  if ($live) {
    $feedback_service = APPLE_TOOLS_PRODUCTION_APNS_FEEDBACK_SERVICE;
  }
  else {
    $feedback_service = APPLE_TOOLS_SANDBOX_APNS_FEEDBACK_SERVICE;
  }

  $fp = @stream_socket_client($feedback_service, $error, $error_string, 60, STREAM_CLIENT_CONNECT, $ctx);

  if (!$fp) {
    watchdog(APPLE_TOOLS_WATCHDOG, 'Failed to connect to feedback server : @error @error_string.', array('@error' => $error, '@error_string' =>  $error_string));
    return NULL;
  }
  while ($devcon = fread($fp, 38)) {
    $arr = unpack("H*", $devcon);
    $rawhex = trim(implode("", $arr));
    $token = drupal_substr($rawhex, 12, 64);
    if (!empty($token)) {
      apple_tools_disable_device($token);
      watchdog(APPLE_TOOLS_WATCHDOG, 'Unregistering Device Token: @token.', array('@token' => $token));
    }
  }
  fclose($fp);
}

/**
 * Function to check if a certificate is ok, secure and present.
 */
function apple_tools_check_certificate($certificate) {
  if (empty($certificate)) {
    watchdog(APPLE_TOOLS_WATCHDOG, 'No certificate given');
    return FALSE;
  }
  else {
    // Check the certificate.
    if (!file_exists($certificate)) {
      watchdog(APPLE_TOOLS_WATCHDOG, 'Error certificate: @certificate does not exist', array('@certificate' => $certificate));
      return FALSE;
    }
    else {
      clearstatcache();
      $certificate_mod = drupal_substr(sprintf('%o', fileperms($certificate)), -3);
      if ($certificate_mod > 644) {
        watchdog(APPLE_TOOLS_WATCHDOG, 'Certificate: @certificate is insecure! Suggest chmod 644.', array('@certificate' => $certificate));
      }
    }
  }
  return TRUE;
}

/**
 * Get the full path of the certificate file for a given message.
 */
function apple_tools_get_certificate($production, $certificate_filename) {
  if (empty($certificate_filename)) {
    return;
  }
  if ($production) {
    $certificate = APPLE_TOOLS_PRODUCTION_CERTIFICATE_LOCATION . $certificate_filename;
  }
  else {
    $certificate = APPLE_TOOLS_SANDBOX_CERTIFICATE_LOCATION . $certificate_filename;
  }
  return $certificate;
}

/**
 * Get the gateway that is to be used for a given message.
 */
function apple_tools_get_gateway($production) {
  if ($production) {
    $gateway = APPLE_TOOLS_PRODUCTION_APNS_GATEWAY;
  }
  else {
    $gateway = APPLE_TOOLS_SANDBOX_APNS_GATEWAY;
  }
  return $gateway;
}

/**
 * This will take the queued messages from the database table, and submit them
 * to apple in turn.
 */
function apple_tools_push_messages() {
  // Need to do this per app, then we can push multiple messages.
  $apps = apple_tools_retrieve_app();
  foreach ($apps as $app) {
    // Get the messages for this application.
    $messages = apple_tools_retrieve_message($app['aid']);
    $status = FALSE;
    // If there are messages to send.
    if (!empty($messages) && apple_tools_check_certificate(apple_tools_get_certificate($app['status'], $app['certificate']))) {
      $opts = array('ssl' => array('local_cert' => apple_tools_get_certificate($app['status'], $app['certificate'])));
      $ctx = stream_context_create($opts);
      // Open the socket.
      $fp = stream_socket_client(apple_tools_get_gateway($app['status']), $error, $error_string, 60, STREAM_CLIENT_CONNECT, $ctx);
      if (!$fp) {
        watchdog(APPLE_TOOLS_WATCHDOG, 'Failed to connect to APNS: @error @error_string.', array('@error' => $error, '@error_string' =>  $error_string));
        return NULL;
      }
      else {
        $payload = '';
        foreach ($messages as $message) {
          $payload .= chr(0) . pack("n", 32) . pack('H*', $message['token']) . pack("n", drupal_strlen($message['message'])) . $message['message'];
        }
        if (!empty($payload)) {
          $fwrite = fwrite($fp, $payload);
          if (!$fwrite) {
            watchdog(APPLE_TOOLS_WATCHDOG, 'Failed to write to stream');
            $status = apple_tools_push_message_failed($messages);
          }
          else {
            watchdog(APPLE_TOOLS_WATCHDOG, 'Wrote to apple with status: @status', array('@status' => $fwrite));
            $status = apple_tools_push_message_success($messages);
          }
        }
      }
      // Close the connection for this app.
      fclose($fp);
      // Watchdog the status of this app.
      watchdog(APPLE_TOOLS_WATCHDOG, '@status in sending messages for app [@aid]', array('@status' => $status, '@aid' => $app['aid']));
      // Irrespective of success or failure check the feedback service.
      apple_tools_check_feedback($app['status'], apple_tools_get_certificate($app['status'], $app['certificate']));
    }
    else {
      watchdog(APPLE_TOOLS_WATCHDOG, 'Sending messages for app [@aid] failed due to certificate check [@certificate] failure', array('@aid' => $app['aid'], '@certificate' => $app['certificate']));
    }
  }
}



/**
 * Update the message on success.
 *
 * @param $message_id
 *   The id of the message to update.
 */
function apple_tools_push_message_success($messages) {
  $placeholders = implode(', ', array_fill(0, count($messages), "%d"));

  $args = array();
  $args[] = APPLE_TOOLS_MESSAGE_SUCCESS;
  foreach ($messages as $message) {
    $args[] = $message['mid'];
  }

  $result = db_query("UPDATE {apple_tools_message}
    SET status = %d
    WHERE mid IN ($placeholders)", $args);
  return t('success');
}

/**
 * Update the message on failure.
 *
 * @param $message_id
 *   The id of the message to update.
 */
function apple_tools_push_message_failed($messages) {
  $placeholders = implode(', ', array_fill(0, count($messagei_ids), "%d"));

  $args = array();
  $args[] = APPLE_TOOLS_MESSAGE_FAILURE;
  foreach ($messages as $message) {
    $args[] = $message['mid'];
  }

  $result = db_query("UPDATE {apple_tools_message}
    SET status = %d
    WHERE mid IN ($placeholders)", $args);
  return t('failed');
}

/**
 * This will allow messages of various formats be submitted to the database.
 *
 * @param $progressive
 *   TRUE if in an admin interface, FALSE if in drush.
 *
 */
function apple_tools_submit_message($to, $app, $delivery, $alert, $badge = NULL, $sound = NULL, $custom = NULL, $progressive = TRUE) {
  if ($to == 'ALL') {
    // Need to do this through a batch process, as too many devices to load into an array.
    $batch = array(
      'title' => 'Queueing messages for devices.',
      'operations' => array(
        array('apple_tools_queue_messages_batch', array($app, $delivery, $alert, $badge, $sound, $custom)),
      ),
      'finished' => 'apple_tools_queue_messages_finished',
      'file' => drupal_get_path('module', 'apple_tools') . '/apple_tools.batch.inc',
    );
    batch_set($batch);
    $batch =& batch_get();
    $batch['progressive'] = $progressive;
    batch_process();    
    return 'Saving messages to the message queue.';
  }
  else {
    $list_devices = explode(',', $to);
  }

  if ($app == 'ALL') {
    $list_apps = array();
    // This message is to go to all apps.
    $apps = apple_tools_retrieve_app();
    foreach ($apps as $app) {
      $list_apps[] = $app['aid'];
    }
  }
  else {
    $list_apps = explode(',', $app);
  }

  $output = '';
  foreach ($list_devices as $device_id) {
    foreach ($list_apps as $app_id) {
      // We are in the loop of apps for devices.
      $result = apple_tools_submit_message_to_device_for_app($device_id, $app_id, $delivery, $alert, $badge, $sound, $custom);
      if (!empty($result)) {
        $output .= t('@result for device @did and app @app_id.', array('@result' => $result, '@did' => $device_id, '@app_id' => $app_id));
      }
    }
  }
  return $output;
}

/**
 * This submits the message to the individual device and app.
 *
 */
function apple_tools_submit_message_to_device_for_app($device_id, $app_id, $delivery, $alert, $badge=NULL, $sound = NULL, $custom = NULL) {
  // Does this device exist, is the app id ok and is it enabled.
  $device_app = apple_tools_retrieve_deviceapp($device_id, $app_id);
  // What notifications do we allow, does it match for this device/app.

  if (!empty($device_app)) {
    $deliver = TRUE;

    $msg = array();
    $msg['aps'] = array();

    if ($device_app['status'] != APPLE_TOOLS_MESSAGE_DEVICE_APP_STATUS_OK) {
      // This app is disabled for this device, dont deliver.
      $deliver = FALSE;
    }
    if (!$device_app['pushsound'] && !$device_app['pushalert'] && !$device_app['pushbadge']) {
      // All notifications off so will not deliver to this user.
      $deliver = FALSE;
    }
    if ($deliver) {
      if ($device_app['pushsound'] && !empty($sound) && is_string($sound)) {
        // Needs to be a string.
        $msg['aps']['sound'] = $sound;
      }
      if ($device_app['pushalert']) {
        // Alert needs to be a structure, or just a plain string.
        switch (TRUE) {
          case(!empty($alert['alert']) && empty($alert['actionlockey']) && empty($alert['lockey']) && empty($alert['locargs'])):
            if (!is_string($alert['alert'])) {
              watchdog(APPLE_TOOLS_WATCHDOG, 'Invalid Alert Format. See documentation for correct procedure.', $severity = WATCHDOG_NOTICE);
            }
            else {
              $msg['aps']['alert'] = (string)$alert['alert'];
            }
            break;

          case(!empty($alert['alert']) && !empty($alert['actionlockey']) && empty($alert['lockey']) && empty($alert['locargs'])):
            if (!is_string($alert['alert'])) {
              watchdog(APPLE_TOOLS_WATCHDOG, 'Invalid Alert Format. See documentation for correct procedure.', $severity = WATCHDOG_NOTICE);
            }
            elseif (!is_string($alert['actionlockey'])) {
              watchdog(APPLE_TOOLS_WATCHDOG, 'Invalid Action Loc Key Format. See documentation for correct procedure.', $severity = WATCHDOG_NOTICE);
            }
            else {
              $msg['aps']['alert']['body'] = (string)$alert['alert'];
              $msg['aps']['alert']['action-loc-key'] = (string)$alert['actionlockey'];
            }
            break;

          case(empty($alert['alert']) && empty($alert['actionlockey']) && !empty($alert['lockey']) && !empty($alert['locargs'])):
            if (!is_string($alert['lockey'])) {
              watchdog(APPLE_TOOLS_WATCHDOG, 'Invalid Loc Key Format. See documentation for correct procedure.', $severity = WATCHDOG_NOTICE);
            }
            else {
              $msg['aps']['alert']['loc-key'] = (string)$alert['lockey'];
              $msg['aps']['alert']['loc-args'] = $alert['locargs'];
            }
            break;

          default:
            watchdog(APPLE_TOOLS_WATCHDOG, 'Invalid Alert Format. See documentation for correct procedure.', $severity = WATCHDOG_NOTICE);
            break;
        }
      }
      if ($device_app['pushbadge'] && !empty($badge) && is_int((int)$badge)) {
        $msg['aps']['badge'] = $badge;
      }
      // Content avaliable values.
      if ($alert['content-available'] == 1) {
        $msg['aps']['content-available'] = 1;
      }

      // Custom payload values.
      if (!empty($custom) && is_array($custom)) {
        foreach (array_keys($custom) as $custom_property) {
          $msg[$custom_property] = $custom[$custom_property];
        }
      }

      // Check the message has some structure in the aps key.
      if (!empty($msg['aps'])) {
        $message = json_encode($msg);
      }
      else {
        return t('nothing to do');
      }

      // Get the device details (to get timezone offset).
      $devices = apple_tools_retrieve_device($device_id);
      if (sizeof($devices) != 1) {
        watchdog(APPLE_TOOLS_WATCHDOG, 'Wrong number of devices returned, should be only one, did:@did', array('@did' => $device_id));
        return t('Message failed');
      }
      // If all ok so far, create message with relavent notification
      // to be delivered at x time for device and app.
      $delivery_time = (!empty($delivery)) ? $delivery:time();

      // Need to add on the GMToffset to the delivery_time.
      if ($devices[0]['timezone_offset'] < 0) {
        // These devices are in the past, so add on the offset.
        $delivery_time += 0 - (60 * 60 * $devices[0]['timezone_offset']);
      }
      elseif ($devices[0]['timezone_offset'] > 0) {
        // Delivery is in the future but still within offset.
        if ($delivery_time > time() + 60 * 60 * $devices[0]['timezone_offset']) {
          $delivery_time -= 60 * 60 * $devices[0]['timezone_offset'];
        }
        // Delivery is more than one day in the future, so remove offset from
        // the delivery time.
        elseif ($delivery_time > (time()+(60 * 60 * 24))) {
          $delivery_time -= 60 * 60 * $devices[0]['timezone_offset'];
        }
        else {
          // Delivery is within the current day so move to tomorrow.
          $delivery_time += 60 * 60 * 24;
          $delivery_time -= 60 * 60 * $devices[0]['timezone_offset'];
        }
      }

      apple_tools_create_message($message, $app_id, $device_id, $delivery_time);

      return t('Message queued');
    }

  }
}

