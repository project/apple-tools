<?php
/**
 * @file
 * Registration page callbacks for apple_tools module.
 */

/**
 * Registration form action.
 */
function apple_tools_register() {
  $app = array(
    'app_id' => $_REQUEST['appid'],
    'version' => $_REQUEST['appversion'],
  );

  $device = array(
    'token' => $_REQUEST['devicetoken'],
    'name' => $_REQUEST['devicename'],
    'model' => $_REQUEST['devicemodel'],
    'version' => $_REQUEST['deviceversion'],
    'GMTtimezone' => $_REQUEST['tz'],
  );

  $deviceapp = array(
    'did' => apple_tools_create_device($device),
    'aid' => apple_tools_create_app($app),
    'status' => 1,
    'pushbadge' => $_REQUEST['pushbadge'],
    'pushalert' => $_REQUEST['pushalert'],
    'pushsound' => $_REQUEST['pushsound'],
  );

  apple_tools_create_deviceapp($deviceapp);

  module_invoke_all('apple_tools_register',
    $app['app_id'],
    $app['version'],
    $device['token'],
    $device['name'],
    $device['model'],
    $device['version'],
    $device['GMTtimezone'],
    $deviceapp['pushbadge'],
    $deviceapp['pushalert'],
    $deviceapp['pushsound']);
}

/**
 * Hook for register to allow other modules to use the register information.
 */
function hook_apple_tools_register($app_id, $appversion, $devicetoken, $devicename, $devicemodel, $deviceversion, $devicetimezone, $allowpushbadge, $allowpushalert, $allowpushsound) {
  return;
}
